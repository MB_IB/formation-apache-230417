# Formation Apache - TP1

## Sujet 

Un site web contient 2 pages utiles, une page avec une présentation, et une page d'affichage d'un texte secret. Les usagers habituellement passent quelques secondes sur la présentation, puis affichent 5 à 20 fois la page des secrets à 10s d'intervale environ (sans cache), puis s'en vont. Ils peuvent être très nombreux, et on cherche à optimiser cette affichage.

La page avec le texte secret est réalisée en partant d'un grand fichier HTML (environ 10Ko), placé dans un sous-répertoire "secret", et en y ajoutant cette configuration, qui nécessite le module "substitute".
```
<Directory /var/www/html/secret>
AddOutputFilterByType SUBSTITUTE text/html
Substitute "s/\w+/./i"
</Directory>
```

Il faut tester le site dans le cadre d'un benchmark efficasse afin de déterminer les options idéales : type de workers (module MPM), options principales de configuration des processus (StartServers, ThreadsPerChilds) et des pages (keepalive, compression).

Le site est un VirtualHost en Apache2.4 sur une VM VirtualBox Debian à jour.

## Modalités

En groupe de 2, réalisation en 1h30 environ.

## Rendu

Rendu sous forme d'une série de grilles de données crée à la main (sur papier, fichier texte...) indiquant le temps moyen pour 99% des requêtes, quand 2000 utilisateurs visitent le site en même temps. Elle devrait contenir 16 à 64 valeurs.

