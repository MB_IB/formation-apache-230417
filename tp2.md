# Formation Apache Web Server - TP2

## Sujet 

Sur le port 81, un site web accessible par nom contient 2 répertoires (ou des URL qui semblent correspondre à un répertoire).
- La requête http://[cesiteamonnom]:81/mespages/index.html renvoie une page HTML indiquant son auteur, moi
- La requête http://[cesiteamonnom]:81/lespages/index.html renvoie une page HTML indiquant son auteur, c'est à dire n'importe lequel de mes 5 voisins, semble-t-il au hasard parmi eux, par un système de reverse-proxy. Si aucun d'entre-eux n'a un site accessible, c'est alors ma page qui est renvoyée.

J'ai 3 fichiers de logs d'accès :
- mes accès à ma page
- mes accès à des pages de mes voisins
- les accès de mes voisins à ma page

Dans ces 2 derniers cas, le voisin doit être identifiable

Enfin :
- la requête http://[cesiteamonnom]:81/[voisin]/index.html me redirige directement (et de façon pérenne) sur le repertoire [mespages] du site du voisin en question
- seuls mes voisins (par exemple, pas la machine de formation) peuvent accéder à http://[cesiteamonnom]:81/mespages/

## Modalités

En groupe de 1, réalisation en 1h environ.

## Rendu

L'accès à mon site, http://[cesiteamonnom]:81/lespages/index.html est possible depuis la machine de formation (dont le fichier hosts est adapté).
